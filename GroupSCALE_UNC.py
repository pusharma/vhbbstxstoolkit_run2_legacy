import ROOT
import math

def GetType(Type_num):
    if Type_num == 3: return   'qqWlvH125'
    elif Type_num == 2: return 'qqZllH125'
    elif Type_num == 4: return 'ggZllH125'
    elif Type_num == 1: return 'qqZvvH125'
    elif Type_num == 5: return 'ggZvvH125'

def getXS(sigName):
    if sigName == 'qqWlvH125': return 0.46227
    elif sigName == 'qqZllH125' : return 0.07704
    elif sigName == 'ggZllH125' : return 0.01242
    elif sigName == 'qqZvvH125' : return 0.15305
    elif sigName == 'ggZvvH125' : return 0.02457


def getResults(input_file):
  theF = open(input_file)
  lines = theF.readlines()
  # to let the loop finish
  lines.append('Bin\n')
  Rows_notDetected = True
  Type = 0
  Results = {}
  for line in lines:
    if 'Bin' in line:
      if Type > 0: 
        TempCon['Bins'] = truthBins
        Results[GetType(Type)] = TempCon
        print(f'TempCon is  {TempCon} Results is {Results}  GetType(Type) is {GetType(Type)}  Type is {Type}')
      Type += 1
      TempCon = {}
      truthBins = []
      if Rows_notDetected:
        Rows = [ x.strip(' ') for x in line.split(",")[1:-1] ]
        Rows_notDetected = False
    if '_PTV_' in line: 
      Numbs = [ x.strip(' ') for x in line.split(",")[1:-1] ]
      truthBin = line.split(",")[0].split('_PTV_')[1]
      TempCon[truthBin] = dict(list(zip(Rows,Numbs)))
      truthBins.append(truthBin)
      #print Numbs
      #exit()
    #  print line
  theF.close()
  return Results,Rows


def getContentByLabel(h, name):
    if debug:
        print("Getting histogram: %s"%name)
    for iBin in range(1, h.GetNbinsX()+1):
        if name == h.GetXaxis().GetBinLabel(iBin):
            return h.GetBinContent(iBin)
    return -1

def getCrossSection(h, name):
    name = name.replace('x','_')
    tmpXS = 0.0
    if "QQZH" in name:
        name = name.replace('QQZH','QQ2HLL')
        tmpXS = getContentByLabel(h, name)
        name = name.replace('QQ2HLL','QQ2HNUNU')
        tmpXS += getContentByLabel(h, name)
    elif "GGZH" in name:
        name = name.replace('GGZH','GG2HLL')
        tmpXS = getContentByLabel(h, name)
        name = name.replace('GG2HLL','GG2HNUNU')
        tmpXS += getContentByLabel(h, name)
    else:
        tmpXS = getContentByLabel(h, name)
    return tmpXS

# def createXSLookupTable(h, groupDic):
#     xsDic = {}
#     for key, value in groupDic.items():
#         tmpXS = 0.0
#         for val in value: 
#             xsDic[val] = getCrossSection(h, val) #fill individual
#             tmpXS+= getCrossSection(h, val)
#         xsDic[key] = tmpXS #fill the group
#     return xsDic

def createXSLookupTable(final_xsec_dict, groupDic):
    xsDic = {}
    for key, value in groupDic.items():
        tmpXS = 0.0
        for val in value: 
            xsDic[val] = final_xsec_dict[val]
            tmpXS+= final_xsec_dict[val]
        xsDic[key] = tmpXS #fill the group
    return xsDic

def getBinNrByLabel(h, name):
    for iBin in range(1, h.GetNbinsX()+1):
        if name == h.GetXaxis().GetBinLabel(iBin):
            return iBin
    return -1

def Convet2DHistToDictionary(Hist1):
  Variation = {}
  nX1 = Hist1.GetXaxis().GetNbins()
  nY1 = Hist1.GetYaxis().GetNbins()
  VarNames = []
  BinNames = []
  for ix in range(1,nX1+1):
    xBinName = Hist1.GetXaxis().GetBinLabel(ix+1)
    BinNames.append(xBinName.replace('_','x'))
    truthBinVar = {}
    for iy in range(1,nY1+1):
      yBinName = Hist1.GetYaxis().GetBinLabel(iy+1)
      content = Hist1.GetBinContent(ix+1,iy+1)
      truthBinVar = content
    Variation[xBinName] = truthBinVar
  return Variation,VarNames,BinNames


def get_final_xsec_dict(merge_rules,Variation,BinNames):
    final_list = {}
    final_xsec_dict = {}
    for i in BinNames:
        if 'FWDH' in i or 'TOTAL' in i:
            continue
        else:
            temp_list = i.split('x')
            for merge_keys,merge_values in merge_rules.items():
                if temp_list[0] in merge_values:
                    if f'{i.replace(temp_list[0]+"x"+temp_list[1],merge_keys)}' in final_list.keys():
                        final_list[f'{i.replace(temp_list[0]+"x"+temp_list[1],merge_keys)}'].append(i)
                    else:
                        final_list[f'{i.replace(temp_list[0]+"x"+temp_list[1],merge_keys)}'] =[]
                        final_list[f'{i.replace(temp_list[0]+"x"+temp_list[1],merge_keys)}'].append(i)

    for i in final_list:
        final_xsec_dict[i]=0.0
        for j in final_list[i]:
            final_xsec_dict[i] = final_xsec_dict[i] + Variation[j.replace('x','_')]

    final_xsec_dict_temp = {}
    for i in final_xsec_dict:
        # print(i.split('x')[-1])
        temp_string=i
        final_xsec_dict_temp[temp_string.replace(i.split('x')[-2],f"{i.split('x')[-2]}PTV")]=final_xsec_dict[i]

    return final_xsec_dict_temp





def fillDic(h, name, dic, xsDic):
    #loop over 2d histogram and fill all unc for this stxs bin name
    nBx = h.GetNbinsX()
    nBy = h.GetNbinsY()
    for iBy in range(1, nBy+1):
        uncName = h.GetYaxis().GetBinLabel(iBy)
        iBx = getBinNrByLabel(h, name)
        if uncName in list(dic.keys()):
            # if debug:
             # for i in range(1,h.GetNbinsX()+1):
            #     if name in h.GetXaxis().GetBinLabel(i):
            #         print(f'found name {name}')
            #     else:
            #         print(f'comparing {h.GetXaxis().GetBinLabel(i)} with {name} failed')
            # print(h.GetXaxis().GetBinLabel(iBx))
            # print("****Uncertainty: %s found in %s and %s with content %f XS: %f"%(uncName, h.GetXaxis().GetBinLabel(iBx), h.GetYaxis().GetBinLabel(iBy),h.GetBinContent(iBx, iBy), xsDic[name]))
            dic[uncName] += h.GetBinContent(iBx, iBy)*xsDic[name]

        else:
            if debug:
                print("----Uncertainty: %s first time in %s and %s with XS %f"%(uncName, h.GetXaxis().GetBinLabel(iBx), h.GetYaxis().GetBinLabel(iBy), xsDic[name]))
            dic[uncName] = h.GetBinContent(iBx, iBy)*xsDic[name]

def scaleDic(uncDic, xsDic, groupName):
    for key, value in uncDic.items():
        # print(key, value, xsDic[groupName])
        uncDic[key] = value/xsDic[groupName]

def getGroupUncertainties(h,groupDic, xsDic):
    bigDic = {}
    #which groups do we need?
    for key, value in groupDic.items():
        if debug:
            print("Group: %s"%key)
        tmpDic = {}
        for val in value: 
            fillDic(h, val, tmpDic, xsDic)
        print('filled dict:',key,val,tmpDic)
        scaleDic(tmpDic, xsDic, key)
        bigDic[key] = tmpDic
        print('Scaled dict:',key,val,tmpDic)
    print(bigDic)
    return bigDic
#----end helper fkt def

#2.2) loop over XS histograms
#----helper fkts
def calculateAndSaveResidual(h, groupDic, grUncDic):
    hname = h.GetName()
    hname = hname.replace('Raw','Scale_UNC')
    hnew = h.Clone(hname)
    nBx = hnew.GetNbinsX()
    nBy = hnew.GetNbinsY()
    for iBy in range(1, nBy+1):
        for iBx in range(1, nBx+1):
            uncName = hnew.GetYaxis().GetBinLabel(iBy)
            binName = hnew.GetXaxis().GetBinLabel(iBx)
            delta = hnew.GetBinContent(iBx, iBy)
            groupDelta = 0.0
            residual = 0.0
            groupName = ""
            for key, value in groupDic.items():
                for val in value:
                    if val == binName:
                        groupName = key
            if groupName != "":
                groupDelta = grUncDic[groupName][uncName]
                binNr = hnew.GetBin(iBx, iBy)
                residual = (delta - groupDelta)
                if abs(residual) < 0.00001:
                    residual = 0.0                
                hnew.SetBinContent(binNr, residual)
            else:
                binNr = hnew.GetBin(iBx, iBy)
                hnew.SetBinContent(binNr, 0.0)

            if groupName != "": 
                print("Getting residual uncertainty")
                print("Uncertainty name: %s"%uncName)
                print("Bin Name: %s"%binName)
                print("Group Name: %s"%groupName)
                print("Delta: %f"%delta)
                print("Delta Group: %f"%groupDelta)
                print("Residual: %f"%residual)
    # temp = hnew.Clone()
    hnew.Write()




res,rows = getResults('/usatlas/u/pusharma/usatlasdata/vhbb/stxs_uncertainity/vhbbstxstoolkit_run2_legacy/QCDScaleVariationCalculation.Nicest.v16/Relative.Error.From.QCD.Variation.With.FrankWith0.5.txt')

res_temp={}
for i in res:
    res_temp[i]={}
    for j in res[i]:
        if j !="Bins":
            temp_string=j
            if "GT600" not in j:
                res_temp[i][temp_string.replace(j.split('_')[1],f"{j.split('_')[1]}PTV")]=res[i][j]
            else:
                res_temp[i][temp_string.replace(j.split('_')[0],f"{j.split('_')[0]}PTV")]=res[i][j]



to_get=['Y','75','150','250','400','600',]
bins=['0_75PTV_0J','0_75PTV_1J','0_75PTV_GE2J','75_150PTV_0J','75_150PTV_1J','75_150PTV_GE2J','150_250PTV_0J','150_250PTV_1J','150_250PTV_GE2J','250_400PTV_0J','250_400PTV_1J','250_400PTV_GE2J','400_600PTV_0J','400_600PTV_1J','400_600PTV_GE2J','GT600PTV_0J','GT600PTV_1J','GT600PTV_GE2J']
signals = ['qqWlvH125', 'ggZvvH125', 'qqZllH125', 'ggZllH125', 'qqZvvH125']

res = ''
res=res_temp.copy()
hist_list={}
for signal in signals:
    hist_list[signal]={}
    for i in to_get:
        hist_list[signal][f'{signal}x{i}']=[]
        for j in bins:
            hist_list[signal][f'{signal}x{i}'].append(res[signal][j][i])
            
            
replacement_dict = {'qqZvvH125': 'QQ2HNUNU',
                    'qqZllH125': 'QQ2HLL',
                    'qqWlvH125': 'QQ2HLNU',
                    'ggZvvH125': 'GG2HNUNU',
                    'ggZllH125': 'GG2HLL',
                    }


delta_1 = {}
delta_2 = {}
for i in res:
    delta_1[i] = []
    delta_2[i] = []
    for j in res[i]:
        if 'Bins' not in j:
            if '600' in f"{j.split('_')[0]}_1":
                delta_1[i].append(res[i][j][f"600_1"])
                delta_2[i].append(res[i][j][f"600_2"])
            else:
                delta_1[i].append(res[i][j][f"{j.split('_')[0]}_1"])
                delta_2[i].append(res[i][j][f"{j.split('_')[0]}_2"])

for i in delta_1:
    hist_list[i][f'{i}x_1']=delta_1[i]
    hist_list[i][f'{i}x_2']=delta_2[i]


f = ROOT.TFile("scaleCheck.root", "RECREATE")
signal = 'qqWlvH125'
hist = ROOT.TH2F(f'Raw', "ALL", len(bins)*3, 0, len(bins)*3+1, len(to_get)+2, 0, len(to_get)+2+1)
hist.SetStats(0)


QQVH={}
h1=hist_list['qqZllH125']
h2=hist_list['qqZvvH125']
for i in h1:
    name=i.replace('qqZllH125x','')
    QQVH[name]=[]
    for number,value in enumerate(h1[i]):
        # print(name,number,value,h2[i.replace('qqZllH125', 'qqZvvH125')][number])
        if value == '-':
            value = 0
        if h2[i.replace('qqZllH125', 'qqZvvH125')][number] == '-':
            h2[i.replace('qqZllH125', 'qqZvvH125')][number] = 0
        updated_value= (getXS('qqZllH125')*float(value)+getXS('qqZvvH125')*float(h2[i.replace('qqZllH125', 'qqZvvH125')][number]))/(getXS('qqZllH125')+getXS('qqZvvH125'))
        QQVH[name].append(updated_value)
GGVH={}
h1=hist_list['ggZllH125']
h2=hist_list['ggZvvH125']
for i in h1:
    name=i.replace('ggZllH125x','')
    GGVH[name]=[]
    for number,value in enumerate(h1[i]):
        # print(number,value,h2[i.replace('qqZllH125', 'qqZvvH125')][number])
        if value == '-':
            value = 0
        if h2[i.replace('ggZllH125', 'ggZvvH125')][number] == '-':
            h2[i.replace('ggZllH125', 'ggZvvH125')][number] = 0
        updated_value= (getXS('ggZllH125')*float(value)+getXS('ggZvvH125')*float(h2[i.replace('ggZllH125', 'ggZvvH125')][number]))/(getXS('ggZllH125')+getXS('ggZvvH125'))
        GGVH[name].append(updated_value)

for i in hist_list[signal]:
    for j,b in enumerate(bins):
        # print(f'QQWHx{b.replace("_", "x").replace("x0J","")}')
        if hist_list[signal][i][j] == '-':
            hist_list[signal][i][j] = 0
        hist.Fill(f'QQWHx{b.replace("_", "x").replace("x0J","x0J")}',f'SysTheoryQCDScaleDelta{i.replace(f"{signal}x", "")}_qqVH', round(float(hist_list[signal][i][j]),4))

for i in QQVH:
    for j,b in enumerate(bins):
        # print(f'QQZHx{b.replace("_", "x").replace("x0J","")}')
        if QQVH[i][j] == '-':
            QQVH[i][j] = 0
        hist.Fill(f'QQZHx{b.replace("_", "x").replace("x0J","x0J")}',f'SysTheoryQCDScaleDelta{i}_qqVH', round(float(QQVH[i][j]),4))  

for i in GGVH:
    for j,b in enumerate(bins):
        # print(f'GGZHx{b.replace("_", "x").replace("x0J","")}')
        if GGVH[i][j] == '-':
            GGVH[i][j] = 0
        hist.Fill(f'GGZHx{b.replace("_", "x").replace("x0J","x0J")}',f'SysTheoryQCDScaleDelta{i}_ggVH', round(float(GGVH[i][j]),4))

hist.Write()
# # Close the ROOT file
f.Close()


fxs = ROOT.TFile("/usatlas/u/pusharma/usatlasdata/vhbb/stxs_uncertainity/vhbbstxstoolkit_run2_legacy/Initial_XS_ade.root", "READ")
grouping ={}
Scheme=14

if Scheme == 14:
    grouping = {
    'ZHx75x150PTVx0J':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", },
    'ZHx75x150PTVxGE1J':{ "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J", },
    'ZHx150x250PTVx0J':{ "QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", },
    'ZHx150x250PTVxGE1J':{"QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTVx0J':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J", },
    'ZHx250x400PTVxGE1J':{ "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", },
    'ZHx400x600PTV':{ "QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", },
    'ZHxGT600PTV':{ "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTV':{ "QQWHx150x250PTVx0J", "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J",},
    'WHx250x400PTV':{ "QQWHx250x400PTVx0J", "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J",  },
    'WHx400x600PTV':{ "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", },
    'WHxGT600PTV':{ "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } }
elif Scheme == 13: 
    grouping = {
    'ZHx75x150PTVx0J':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", },
    'ZHx75x150PTVxGE1J':{ "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J", },
    'ZHx150x250PTVx0J':{ "QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", },
    'ZHx150x250PTVxGE1J':{"QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTVx0J':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J", },
    'ZHx250x400PTVxGE1J':{ "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", },
    'ZHx400x600PTV':{ "QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", },
    'ZHxGT600PTV':{ "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTVx0J':{ "QQWHx150x250PTVx0J", },
    'WHx150x250PTVxGE1J':{ "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J", },
    'WHx250x400PTVx0J':{ "QQWHx250x400PTVx0J", },
    'WHx250x400PTVxGE1J':{ "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J", },
    'WHx400x600PTV':{ "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", },
    'WHxGT600PTV':{ "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } }
elif Scheme == 10:
    grouping = {
    'ZHx75x150PTV':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J",},
    'ZHx150x250PTV':{"QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", "QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTV':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J","QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J",},
    'ZHx400x600PTV':{ "QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", },
    'ZHxGT600PTV':{ "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTV':{ "QQWHx150x250PTVx0J", "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J",},
    'WHx250x400PTV':{ "QQWHx250x400PTVx0J", "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J",  },
    'WHx400x600PTV':{ "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", },
    'WHxGT600PTV':{ "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } }
elif Scheme == 133:
    grouping = {
    'ZHx75x150PTVx0J':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", },
    'ZHx75x150PTVxGE1J':{ "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J", },
    'ZHx150x250PTVx0J':{ "QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", },
    'ZHx150x250PTVxGE1J':{"QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTVx0J':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J", },
    'ZHx250x400PTVxGE1J':{ "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", },
    'ZHxGT400PTV':{"QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTVx0J':{ "QQWHx150x250PTVx0J", },
    'WHx150x250PTVxGE1J':{ "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J", },
    'WHx250x400PTVx0J':{ "QQWHx250x400PTVx0J", },
    'WHx250x400PTVxGE1J':{ "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J", },
    'WHxGT400PTV':{ "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J","QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } }
elif Scheme == 8:
    grouping = {
    'ZHx75x150PTV':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J",},
    'ZHx150x250PTV':{"QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", "QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTV':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J","QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J",},
    'ZHxGT400PTV':{"QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTV':{ "QQWHx150x250PTVx0J", "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J",},
    'WHx250x400PTV':{ "QQWHx250x400PTVx0J", "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J",  },
    'WHxGT400PTV':{"QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } 
    }
elif Scheme == 2:
    grouping = {
    'ZHbb':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J",
    "QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", "QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J",
    "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J","QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J",
    "QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHbb':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J",
    "QQWHx150x250PTVx0J", "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J",
    "QQWHx250x400PTVx0J", "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J",
    "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } 
    }
   
    
Scheme_poi={13:'15',14:'13',10:'10',133:'11p2',8:'8',2:'2'}   
    
outputName = f"out_Scale_{Scheme_poi[Scheme]}POI_legacy_scheme2.root"



merge_rules= {'QQZH':['QQ2HLL','QQ2HNUNU'],
'GGZH':['GG2HLL','GG2HNUNU'],
'QQWH':['QQ2HLNU']}

debug = False
hXS = fxs.Get("Initial")
Variation,VarNames,BinNames= Convet2DHistToDictionary(hXS)
final_xsec_dict = get_final_xsec_dict(merge_rules,Variation,BinNames)
infile=ROOT.TFile.Open('scaleCheck.root')
hunc = infile.Get("Raw")
xsLookup = createXSLookupTable(final_xsec_dict, grouping)
groupUncDic = getGroupUncertainties(hunc,grouping, xsLookup)
fout = ROOT.TFile(outputName,"recreate")
hunc.Write()
#----end helper fkts
calculateAndSaveResidual(hunc, grouping, groupUncDic)
fout.Close()


for key, value in groupUncDic.items():
    print("Uncertainty for bin: %s"%key)
    unc2 = 0.0
    for unName, val in value.items():
        unc2 += val*val
    unc1 = math.sqrt(unc2)
    xsval = xsLookup[key]
    print("---> %f for a XS of %f"%(unc1, xsval))
    # print("%f"%(unc1))