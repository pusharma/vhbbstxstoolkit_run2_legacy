import ROOT

variation = ["PDF01", "PDF02", "PDF03", "PDF04", "PDF05", "PDF06", "PDF07", "PDF08", "PDF09", "PDF10", "PDF11", "PDF12", "PDF13", "PDF14", "PDF15", "PDF16", "PDF17", "PDF18", "PDF19", "PDF20", "PDF21", "PDF22", "PDF23", "PDF24", "PDF25", "PDF26", "PDF27", "PDF28", "PDF29", "PDF30", "alphaSup","alphaSdn"]

# infile=ROOT.TFile.Open("/gpfs/mnt/atlasgpfs01/usatlas/data/pusharma/vhbb/stxs_uncertainity/inputs/hadded_noreco_nominal_19072024.root")
infile=ROOT.TFile.Open("/gpfs/mnt/atlasgpfs01/usatlas/data/pusharma/vhbb/stxs_uncertainity/inputs/hadded_noreco_nominal_noPDFRW_22072024.root")
h0_name='ggZllH125_Stage1Bin_RecoMapEPSNicest'
hists=['ggZvvH125_Stage1Bin_RecoMapEPSNicest','qqWlvH125_Stage1Bin_RecoMapEPSNicest','qqZllH125_Stage1Bin_RecoMapEPSNicest','qqZvvH125_Stage1Bin_RecoMapEPSNicest']
hist_nom=infile.Get(f'PDF00/{h0_name}')
for i in hists:
    hist_nom.Add(infile.Get(f'PDF00/{i}'))
hist_list={}

for i in variation:
    # print(f'{i}/{h0_name}')
    h_temp=infile.Get(f'{i}/{h0_name}').Clone()
    for j in hists:
        # print(f'{i}/{j}')
        h_temp.Add(infile.Get(f'{i}/{j}'))
    hist_list[i]=h_temp

hist_list['alphaS'] = hist_list['alphaSup'].Clone()
for i in range(1, hist_list['alphaS'].GetNbinsX()+1):
    hist_list['alphaS'].SetBinContent(i, ((abs(hist_list['alphaSup'].GetBinContent(i)) - hist_nom.GetBinContent(i)) + abs(hist_list['alphaSdn'].GetBinContent(i)-hist_nom.GetBinContent(i)))/(2*hist_nom.GetBinContent(i)))
    hist_list['alphaS'].SetBinError(i, 0.0)



hist_list.pop('alphaSup')
hist_list.pop('alphaSdn')


for i in hist_list:
    if i == 'alphaS':
        continue
    for j in range(1, hist_nom.GetNbinsX()+1):
        if hist_nom.GetBinContent(j) == 0:
            hist_list[i].SetBinContent(j,0)
            hist_list[i].SetBinError(j,0)
        else:
            hist_list[i].SetBinContent(j,-1 + hist_list[i].GetBinContent(j)/hist_nom.GetBinContent(j))
            hist_list[i].SetBinError(j,-1 + hist_list[i].GetBinError(j)/hist_nom.GetBinContent(j))

# for i in range(1, hist_nom.GetNbinsX()+1):
#     if "QQ2HLNU" in hist_nom.GetXaxis().GetBinLabel(i):
#         print(f"'{hist_nom.GetXaxis().GetBinLabel(i).replace('QQ2HLNU', '')}',",end=' ')
        
def getXS(sigName):
  if sigName == 'QQ2HLNU': return 0.46227
  elif sigName == 'QQ2HLL' : return 0.07704
  elif sigName == 'GG2HLL' : return 0.01242
  elif sigName == 'QQ2HNUNU' : return 0.15305
  elif sigName == 'GG2HNUNU' : return 0.02457
  
signals = ['QQ2HLNU','QQ2HLL','QQ2HNUNU','GG2HLL','GG2HNUNU']

groupings={'qqZH':['QQ2HLL','QQ2HNUNU'],
           'ggZH':['GG2HLL','GG2HNUNU'],
           'WH':['QQ2HLNU']}

max_split_old=[ '_PTV_0_75_0J', '_PTV_0_75_1J', '_PTV_0_75_GE2J', '_PTV_75_150_0J', '_PTV_75_150_1J', '_PTV_75_150_GE2J', '_PTV_150_250_0J', '_PTV_150_250_1J', '_PTV_150_250_GE2J', '_PTV_250_400_0J', '_PTV_250_400_1J', '_PTV_250_400_GE2J', '_PTV_400_600_0J', '_PTV_400_600_1J', '_PTV_400_600_GE2J', '_PTV_GT600_0J', '_PTV_GT600_1J', '_PTV_GT600_GE2J','_FWDH']
max_split_new=[ '_0_75PTV_0J', '_0_75PTV_1J', '_0_75PTV_GE2J', '_75_150PTV_0J', '_75_150PTV_1J', '_75_150PTV_GE2J', '_150_250PTV_0J', '_150_250PTV_1J', '_150_250PTV_GE2J', '_250_400PTV_0J', '_250_400PTV_1J', '_250_400PTV_GE2J', '_400_600PTV_0J', '_400_600PTV_1J', '_400_600PTV_GE2J', '_GT600PTV_0J', '_GT600PTV_1J', '_GT600PTV_GE2J','_FWDH']
max_split=max_split_new
mapping_dict = {old: new for old, new in zip(max_split_old, max_split_new)}
hist_dict={}
for signal in signals:
    hist = ROOT.TH2F(f'{signal}', "PDFDiff2DMerge", len(max_split), 0, len(max_split)+1, len(hist_list.keys()), 0, len(hist_list.keys())+1)
    for i, label in enumerate(max_split, start=1):
        hist.GetXaxis().SetBinLabel(i, label)

    for i, label in enumerate(hist_list.keys(), start=1):
        hist.GetYaxis().SetBinLabel(i, label)

    for i in hist_list:
        for j in range(1, hist_nom.GetNbinsX()+1):
            if signal in hist_nom.GetXaxis().GetBinLabel(j):
                hist.Fill(hist.GetXaxis().FindBin(mapping_dict[hist_nom.GetXaxis().GetBinLabel(j).replace(f'{signal}', '')]), hist.GetYaxis().FindBin(i), hist_list[i].GetBinContent(j))
    hist_dict[signal]=hist



hist_rename={"QQ2HLL":"QQZH","GG2HLL":"GGZH","QQ2HLNU":"QQWH"}

 
def Merge2DHist(Hist1,Hist2,xs1,xs2,histName,max_split,hist_list):
    nX1 = Hist1.GetXaxis().GetNbins()
    nY1 = Hist1.GetYaxis().GetNbins()
    nX2 = Hist2.GetXaxis().GetNbins()
    nY2 = Hist2.GetYaxis().GetNbins()
    if   histName == 'WH':   prefix = 'QQ2HLNU'
    elif histName == 'qqZH': prefix = 'QQ2HLL'
    elif histName == 'ggZH': prefix = 'GG2HLL'

    if nX1 != nX2:
        print('The numbers of x axis are not equal! Existing')
        exit()
    if nY1 != nY2:
        print('The numbers of y axis are not equal! Existing')
        exit()
    hist = ROOT.TH2F(f'PDF_for_{histName}', "PDFDiff2DMerge", len(max_split), 0, len(max_split)+1, len(hist_list.keys()), 0, len(hist_list.keys())+1)
    hist.SetStats(0)
    for ix in range(nX1):
        xBinName = Hist1.GetXaxis().GetBinLabel(ix+1)
        hist.GetXaxis().SetBinLabel(ix+1,hist_rename[prefix]+xBinName.replace('_', 'x')) 
        for iy in range(nY1):
            if ix == 0:
                yBinName = Hist1.GetYaxis().GetBinLabel(iy+1)
                hist.GetYaxis().SetBinLabel(iy+1,yBinName.replace('PDF', 'SysTheoryPDF_').replace('alphaS', 'SysTheoryalphas'))
            content1 = Hist1.GetBinContent(ix+1,iy+1)
            content2 = Hist2.GetBinContent(ix+1,iy+1)
            contentMerge = ( content1*xs1 + content2*xs2 )/( xs1 + xs2 )
            hist.SetBinContent( ix+1, iy+1, contentMerge )
    return hist


def Fixhist(Hist1,histName,max_split,hist_list):
    nX1 = Hist1.GetXaxis().GetNbins()
    nY1 = Hist1.GetYaxis().GetNbins()
    if   histName == 'WH':   prefix = 'QQ2HLNU'
    hist = ROOT.TH2F(f'PDF_for_{histName}', "PDFDiff2DMerge", len(max_split), 0, len(max_split)+1, len(hist_list.keys()), 0, len(hist_list.keys())+1)
    hist.SetStats(0)
    for ix in range(nX1):
        xBinName = Hist1.GetXaxis().GetBinLabel(ix+1)
        hist.GetXaxis().SetBinLabel(ix+1,hist_rename[prefix]+xBinName.replace('_', 'x')) 
        for iy in range(nY1):
            if ix == 0:
                yBinName = Hist1.GetYaxis().GetBinLabel(iy+1)
                hist.GetYaxis().SetBinLabel(iy+1,yBinName.replace('PDF', 'SysTheoryPDF_').replace('alphaS', 'SysTheoryalphas'))
            content1 = Hist1.GetBinContent(ix+1,iy+1)
            contentMerge = content1
            hist.SetBinContent( ix+1, iy+1, contentMerge )
    return hist


hQQZH=Merge2DHist(hist_dict['QQ2HLL'],hist_dict['QQ2HNUNU'],getXS('QQ2HLL'),getXS('QQ2HNUNU'),'qqZH',max_split,hist_list)
hGGZH=Merge2DHist(hist_dict['GG2HLL'],hist_dict['GG2HNUNU'],getXS('GG2HLL'),getXS('GG2HNUNU'),'ggZH',max_split,hist_list)
hQQWH=Fixhist(hist_dict['QQ2HLNU'],'WH',max_split,hist_list)





f = ROOT.TFile("out_PDF_raw.root", "RECREATE")
hQQZH.Write()
hGGZH.Write()
hQQWH.Write()
f.Close()



import math

fin = ROOT.TFile("out_PDF_raw.root", "READ")


debug = False


h2dQQWHin = fin.Get("PDF_for_WH")
h2dQQZHin = fin.Get("PDF_for_qqZH")
h2dGGZHin = fin.Get("PDF_for_ggZH")

unc = {
    "WH": h2dQQWHin,
    "QQZH": h2dQQZHin,
    "GGZH": h2dGGZHin }


fxs = ROOT.TFile("/usatlas/u/pusharma/usatlasdata/vhbb/stxs_uncertainity/vhbbstxstoolkit_run2_legacy/Initial_XS_ade.root", "READ")
Scheme=13

if Scheme == 14:
    grouping = {
    'ZHx75x150PTVx0J':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", },
    'ZHx75x150PTVxGE1J':{ "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J", },
    'ZHx150x250PTVx0J':{ "QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", },
    'ZHx150x250PTVxGE1J':{"QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTVx0J':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J", },
    'ZHx250x400PTVxGE1J':{ "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", },
    'ZHx400x600PTV':{ "QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", },
    'ZHxGT600PTV':{ "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTV':{ "QQWHx150x250PTVx0J", "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J",},
    'WHx250x400PTV':{ "QQWHx250x400PTVx0J", "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J",  },
    'WHx400x600PTV':{ "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", },
    'WHxGT600PTV':{ "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } }
elif Scheme == 13: 
    grouping = {
    'ZHx75x150PTVx0J':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", },
    'ZHx75x150PTVxGE1J':{ "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J", },
    'ZHx150x250PTVx0J':{ "QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", },
    'ZHx150x250PTVxGE1J':{"QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTVx0J':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J", },
    'ZHx250x400PTVxGE1J':{ "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", },
    'ZHx400x600PTV':{ "QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", },
    'ZHxGT600PTV':{ "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTVx0J':{ "QQWHx150x250PTVx0J", },
    'WHx150x250PTVxGE1J':{ "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J", },
    'WHx250x400PTVx0J':{ "QQWHx250x400PTVx0J", },
    'WHx250x400PTVxGE1J':{ "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J", },
    'WHx400x600PTV':{ "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", },
    'WHxGT600PTV':{ "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } }
elif Scheme == 10:
    grouping = {
    'ZHx75x150PTV':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J",},
    'ZHx150x250PTV':{"QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", "QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTV':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J","QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J",},
    'ZHx400x600PTV':{ "QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", },
    'ZHxGT600PTV':{ "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTV':{ "QQWHx150x250PTVx0J", "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J",},
    'WHx250x400PTV':{ "QQWHx250x400PTVx0J", "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J",  },
    'WHx400x600PTV':{ "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", },
    'WHxGT600PTV':{ "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } }
elif Scheme == 133:
    grouping = {
    'ZHx75x150PTVx0J':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", },
    'ZHx75x150PTVxGE1J':{ "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J", },
    'ZHx150x250PTVx0J':{ "QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", },
    'ZHx150x250PTVxGE1J':{"QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTVx0J':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J", },
    'ZHx250x400PTVxGE1J':{ "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", },
    'ZHxGT400PTV':{"QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTVx0J':{ "QQWHx150x250PTVx0J", },
    'WHx150x250PTVxGE1J':{ "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J", },
    'WHx250x400PTVx0J':{ "QQWHx250x400PTVx0J", },
    'WHx250x400PTVxGE1J':{ "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J", },
    'WHxGT400PTV':{ "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J","QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } }
elif Scheme == 8:
    grouping = {
    'ZHx75x150PTV':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J",},
    'ZHx150x250PTV':{"QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", "QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTV':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J","QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J",},
    'ZHxGT400PTV':{"QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTV':{ "QQWHx150x250PTVx0J", "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J",},
    'WHx250x400PTV':{ "QQWHx250x400PTVx0J", "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J",  },
    'WHxGT400PTV':{"QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } 
    }
elif Scheme == 2:
    grouping = {
    'ZHbb':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J",
    "QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", "QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J",
    "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J","QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J",
    "QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHbb':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J",
    "QQWHx150x250PTVx0J", "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J",
    "QQWHx250x400PTVx0J", "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J",
    "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } 
    }
   
    
Scheme_poi={13:'15',14:'13',10:'10',133:'11p2',8:'8',2:'2'}

outName = f"out_PDF_{Scheme_poi[Scheme]}POI_legacy.root"

def getContentByLabel(h, name):
    if debug:
        print("Getting histogram: %s"%name)
    for iBin in range(1, h.GetNbinsX()+1):
        if name == h.GetXaxis().GetBinLabel(iBin):
            return h.GetBinContent(iBin)
    return -1

def getCrossSection(h, name):
    name = name.replace('x','_')
    tmpXS = 0.0
    if "QQZH" in name:
        name = name.replace('QQZH','QQ2HLL')
        tmpXS = getContentByLabel(h, name)
        name = name.replace('QQ2HLL','QQ2HNUNU')
        tmpXS += getContentByLabel(h, name)
    elif "GGZH" in name:
        name = name.replace('GGZH','GG2HLL')
        tmpXS = getContentByLabel(h, name)
        name = name.replace('GG2HLL','GG2HNUNU')
        tmpXS += getContentByLabel(h, name)
    else:
        tmpXS = getContentByLabel(h, name)
    return tmpXS

def createXSLookupTable(h, groupDic):
    xsDic = {}
    for key, value in groupDic.items():
        tmpXS = 0.0
        for val in value: 
            xsDic[val] = getCrossSection(h, val) #fill individual
            tmpXS+= getCrossSection(h, val)
        xsDic[key] = tmpXS #fill the group
    return xsDic

# def createXSLookupTable(final_xsec_dict, groupDic):
#     xsDic = {}
#     for key, value in groupDic.items():
#         tmpXS = 0.0
#         for val in value: 
#             xsDic[val] = final_xsec_dict[val]
#             tmpXS+= final_xsec_dict[val]
#         xsDic[key] = tmpXS #fill the group
#     return xsDic

def getBinNrByLabel(h, name):
    for iBin in range(1, h.GetNbinsX()+1):
        if name == h.GetXaxis().GetBinLabel(iBin):
            return iBin
    return -1

def Convet2DHistToDictionary(Hist1):
  Variation = {}
  nX1 = Hist1.GetXaxis().GetNbins()
  nY1 = Hist1.GetYaxis().GetNbins()
  VarNames = []
  BinNames = []
  for ix in range(1,nX1+1):
    xBinName = Hist1.GetXaxis().GetBinLabel(ix+1)
    BinNames.append(xBinName.replace('_','x'))
    truthBinVar = {}
    for iy in range(1,nY1+1):
      yBinName = Hist1.GetYaxis().GetBinLabel(iy+1)
      content = Hist1.GetBinContent(ix+1,iy+1)
      truthBinVar = content
    Variation[xBinName] = truthBinVar
  return Variation,VarNames,BinNames


def get_final_xsec_dict(merge_rules,Variation,BinNames):
    final_list = {}
    final_xsec_dict = {}
    for i in BinNames:
        if 'FWDH' in i or 'TOTAL' in i:
            continue
        else:
            temp_list = i.split('x')
            for merge_keys,merge_values in merge_rules.items():
                if temp_list[0] in merge_values:
                    if f'{i.replace(temp_list[0]+"x"+temp_list[1],merge_keys)}' in final_list.keys():
                        final_list[f'{i.replace(temp_list[0]+"x"+temp_list[1],merge_keys)}'].append(i)
                    else:
                        final_list[f'{i.replace(temp_list[0]+"x"+temp_list[1],merge_keys)}'] =[]
                        final_list[f'{i.replace(temp_list[0]+"x"+temp_list[1],merge_keys)}'].append(i)

    for i in final_list:
        final_xsec_dict[i]=0.0
        for j in final_list[i]:
            final_xsec_dict[i] = final_xsec_dict[i] + Variation[j.replace('x','_')]
  
    final_xsec_dict_temp = {}
    for i in final_xsec_dict:
        # print(i.split('x')[-1])
        temp_string=i
        final_xsec_dict_temp[temp_string.replace(i.split('x')[-2],f"{i.split('x')[-2]}PTV")]=final_xsec_dict[i]

    return final_xsec_dict_temp





def fillDic(h, name, dic, xsDic):
    # print(h.GetName())
    #loop over 2d histogram and fill all unc for this stxs bin name
    nBx = h.GetNbinsX()
    nBy = h.GetNbinsY()
    for iBy in range(1, nBy+1):
        uncName = h.GetYaxis().GetBinLabel(iBy)
        # print(uncName)
        iBx = getBinNrByLabel(h, name)
        # for i in range(1,h.GetNbinsX()+1):
        #     if name in h.GetXaxis().GetBinLabel(i):
        #         print(f'found name {name}')
        #     else:
        #         print(f'comparing {h.GetXaxis().GetBinLabel(i)} with {name} failed')
        if uncName in list(dic.keys()):
            if debug:
                print("****Uncertainty: %s found in %s and %s with XS %f"%(uncName, h.GetXaxis().GetBinLabel(iBx), h.GetYaxis().GetBinLabel(iBy), xsDic[name]))
            dic[uncName] += h.GetBinContent(iBx, iBy)*xsDic[name]
        else:
            if debug:
                print("----Uncertainty: %s first time in %s and %s with XS %f"%(uncName, h.GetXaxis().GetBinLabel(iBx), h.GetYaxis().GetBinLabel(iBy), xsDic[name]))
            dic[uncName] = h.GetBinContent(iBx, iBy)*xsDic[name]

def scaleDic(uncDic, xsDic, groupName):
    for key, value in uncDic.items():
        # print(key, value, xsDic[groupName])
        uncDic[key] = value/xsDic[groupName]

def createXSLookupTableNew(final_xsec_dict, groupDic):
    xsDic = {}
    for key, value in groupDic.items():
        tmpXS = 0.0
        for val in value: 
            xsDic[val] = final_xsec_dict[val]
            tmpXS+= final_xsec_dict[val]
        xsDic[key] = tmpXS #fill the group
    return xsDic

def getGroupUncertainties(uncDic, groupDic, xsDic):
    bigDic = {}
    #which groups do we need?
    for key, value in groupDic.items():
        if debug:
            print("Group: %s"%key)
        tmpDic = {}
        for val in value:
            
            if debug:
                print("-->getting unc. for %s"%val)
            if "QQWH" in val:
                htmp = uncDic["WH"]
            elif "QQZH" in val or "QQ2HLL" in val or "QQ2HNUNU" in val:
                htmp = uncDic["QQZH"]
            elif "GGZH" in val or "GG2HLL" in val or "GG2HNUNU" in val:
                htmp = uncDic["GGZH"]
            # print(f"val: {val} tmp: {tmpDic}")
            fillDic(htmp, val, tmpDic, xsDic)

        scaleDic(tmpDic, xsDic, key)
        bigDic[key] = tmpDic

    return bigDic
#----end helper fkt def

#2.2) loop over XS histograms
#----helper fkts
def calculateAndSaveResidual(h, groupDic, grUncDic):
    hname = h.GetName()
    hname = hname.replace('Raw','Scale_UNC')
    hnew = h.Clone(hname)
    nBx = hnew.GetNbinsX()
    nBy = hnew.GetNbinsY()
    for iBy in range(1, nBy+1):
        for iBx in range(1, nBx+1):
            uncName = hnew.GetYaxis().GetBinLabel(iBy)
            binName = hnew.GetXaxis().GetBinLabel(iBx)
            delta = hnew.GetBinContent(iBx, iBy)
            groupDelta = 0.0
            residual = 0.0
            groupName = ""
            for key, value in groupDic.items():
                for val in value:
                    if val == binName:
                        groupName = key
            if groupName != "":
                groupDelta = grUncDic[groupName][uncName]
                binNr = hnew.GetBin(iBx, iBy)
                residual = (delta - groupDelta)
                if abs(residual) < 0.00001:
                    residual = 0.0                
                hnew.SetBinContent(binNr, residual)
            else:
                binNr = hnew.GetBin(iBx, iBy)
                hnew.SetBinContent(binNr, 0.0)

            if groupName != "": 
                print("Getting residual uncertainty")
                print("Uncertainty name: %s"%uncName)
                print("Bin Name: %s"%binName)
                print("Group Name: %s"%groupName)
                print("Delta: %f"%delta)
                print("Delta Group: %f"%groupDelta)
                print("Residual: %f"%residual)
    # temp = hnew.Clone()
    hnew.Write()

    
merge_rules= {'QQZH':['QQ2HLL','QQ2HNUNU'],
'GGZH':['GG2HLL','GG2HNUNU'],
'QQWH':['QQ2HLNU']}

debug = False
hXS = fxs.Get("Initial")
Variation,VarNames,BinNames= Convet2DHistToDictionary(hXS)
final_xsec_dict = get_final_xsec_dict(merge_rules,Variation,BinNames)
xsLookup = createXSLookupTableNew(final_xsec_dict, grouping)
groupUncDic = getGroupUncertainties(unc, grouping, xsLookup)

fout = ROOT.TFile(outName,"recreate")

for key, value in unc.items():
    if debug:
        print("Calculating and saving residuals for %s"%key)
    calculateAndSaveResidual(value, grouping, groupUncDic)

fout.Close()



for key, value in groupUncDic.items():
    print("Uncertainty for bin: %s"%key)
    unc2 = 0.0
    for unName, val in value.items():
        unc2 += val*val
    unc1 = math.sqrt(unc2)
    xsval = xsLookup[key]
    print("---> %f for a XS of %f"%(unc1, xsval))