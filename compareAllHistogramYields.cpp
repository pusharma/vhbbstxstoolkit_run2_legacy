#include <TFile.h>
#include <TH1.h>
#include <TKey.h>
#include <iostream>
#include <fstream>

void compareAllHistogramYields() 
{
    // Open the ROOT files
    TFile *file1 = TFile::Open("/gpfs/mnt/atlasgpfs01/usatlas/data/pusharma/vhbb/stxs_uncertainity/inputs/hadded_nominal_noReco_18032024.root");
    TFile *file2 = TFile::Open("/usatlas/u/pusharma/usatlasdata/vhbb/stxs_uncertainity/inputs/hadded_noReco_25032024.root");

    // Open the output file
    std::ofstream outfile("comparison.txt");
    bool doSyst = false;
    // Iterate over all keys (objects) in the first file
    TIter next(file1->GetListOfKeys());
    TKey *key;
    while ((key = (TKey*)next())) 
    {
        // Get the name of the object
        std::string name = key->GetName();

        // Try to get the object from both files
        TObject *obj1 = file1->Get(name.c_str());
        TObject *obj2 = file2->Get(name.c_str());

        // If either object is null, skip this object
        if (!obj1 || !obj2) continue;

        if (obj1->IsA() == TDirectoryFile::Class() && obj2->IsA() == TDirectoryFile::Class() && doSyst) 
        {   
            // If the objects are directories, iterate over the keys in the directory
            TDirectoryFile *dir1 = (TDirectoryFile*)obj1;
            TDirectoryFile *dir2 = (TDirectoryFile*)obj2;
            outfile <<"\n\n\n"<< name<<std::endl;
            TIter nextDir(dir1->GetListOfKeys());
            TKey *keyDir;
            while ((keyDir = (TKey*)nextDir())) 
            {
                std::string nameDir = keyDir->GetName();

                TObject *objDir1 = dir1->Get(nameDir.c_str());
                TObject *objDir2 = dir2->Get(nameDir.c_str());

                if (!objDir1 || !objDir2 || !objDir1->InheritsFrom("TH1") || !objDir2->InheritsFrom("TH1")) continue;

                TH1F *histDir1 = (TH1F*)objDir1;
                TH1F *histDir2 = (TH1F*)objDir2;

                double yieldDir1 = histDir1->GetEntries();
                double yieldDir2 = histDir2->GetEntries();

                double comparisonDir = yieldDir1 / yieldDir2;

                // Print the comparison
                std::cout << name <<"/" << nameDir << ": " << "Nominal : " <<yieldDir1 <<" Alternative : " << yieldDir2 << " Ratio: " << comparisonDir << std::endl;

                // Save the comparison to the file
                outfile << nameDir << ": " << "Nominal : " <<yieldDir1 <<" Alternative : " << yieldDir2 << " Ratio: " << comparisonDir << std::endl;
            }
        } 
        else if (obj1->InheritsFrom("TH1") && obj2->InheritsFrom("TH1")) 
        {
            // If the objects are histograms, compare the yields
            TH1F *hist1 = (TH1F*)obj1;
            TH1F *hist2 = (TH1F*)obj2;

            // double yield1 = hist1->Integral();
            // double yield2 = hist2->Integral();
            double yield1 = hist1->GetEntries();
            double yield2 = hist2->GetEntries();

            double comparison = yield1 / yield2;

            // Print the comparison
            // std::cout  <<name << ": " << "Nominal : " <<yield1 <<" Alternative : " << yield2 << " Ratio: " << comparison << std::endl;

            // Save the comparison to the file
            if (doSyst){
                if (!(name.find("_NoWeight") != std::string::npos)) {

                    outfile  <<" NOM " << name << ": " << "Nominal : " <<yield1 <<" Alternative : " << yield2 << " Ratio: " << comparison << std::endl;
                }
            }
            else{
                if (!(name.find("_NoWeight") != std::string::npos)) {
                 outfile<< name << ": " << "Nominal : " <<yield1 / 140. <<" Alternative : " << yield2 / 140.<< " Ratio: " << comparison << std::endl;
                }
            }
        }
    }

    // Close the output file
    outfile.close();

    // Clean up
    delete file1;
    delete file2;
}


    