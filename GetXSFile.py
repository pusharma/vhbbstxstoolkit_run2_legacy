import ROOT
# infile=ROOT.TFile.Open("/gpfs/mnt/atlasgpfs01/usatlas/data/pusharma/vhbb/stxs_uncertainity/inputs/hadded_noreco_nominal_noPDFRW_22072024.root") 
infile=ROOT.TFile.Open("/gpfs/mnt/atlasgpfs01/usatlas/data/pusharma/vhbb/stxs_uncertainity/inputs/hadded_nominal_noreco_06062024.root") 

signal_list = ['ggZvvH125_Stage1Bin_RecoMapEPSNicest','ggZllH125_Stage1Bin_RecoMapEPSNicest','qqWlvH125_Stage1Bin_RecoMapEPSNicest','qqZvvH125_Stage1Bin_RecoMapEPSNicest','qqZllH125_Stage1Bin_RecoMapEPSNicest']
sigs = ['ggZvvH125','ggZllH125', 'qqZvvH125','qqZllH125','qqWlvH125']
h1=infile.Get(f'{sigs[0]}_Stage1Bin_RecoMapEPSNicest').Clone()
for i in sigs[1:]:
    h1.Add(infile.Get(f'{i}_Stage1Bin_RecoMapEPSNicest'))

h1.Scale(1/140)
h1.SetName("Initial")
f = ROOT.TFile("Initial_XS_ade.root", "RECREATE")
h1.Write()
f.Close()