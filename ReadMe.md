# To Get the QCD scale STXS uncertainties.

- QCDScaleError.py
- ReadQCDScale.py
- GroupSCALE_UNC.py


# To Get the PDF STXS uncertainties.

- GetPDFResidual.py


# To Get the PS STXS uncertainties.

- GetPSResidual.py

