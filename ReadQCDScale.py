# this script is plotting the 2d map for the QCD scale variation
import ROOT
import math
from collections import OrderedDict

# some ROOT setting
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetPaintTextFormat("1.3f")

def GetType(Type_num):
    if Type_num == 3: return   'qqWlvH125'
    elif Type_num == 2: return 'qqZllH125'
    elif Type_num == 4: return 'ggZllH125'
    elif Type_num == 1: return 'qqZvvH125'
    elif Type_num == 5: return 'ggZvvH125'

def getXS(sigName):
  if   sigName == 'qqWlvH125M': return 0.17949
  elif sigName == 'qqWlvH125P': return 0.28278
  elif sigName == 'qqWlvH125': return 0.46227
  elif sigName == 'qqZllH125' : return 0.07704
  elif sigName == 'ggZllH125' : return 0.01242
  elif sigName == 'qqZvvH125' : return 0.15305
  elif sigName == 'ggZvvH125' : return 0.02457

def getResults(input_file):
  theF = open(input_file)
  lines = theF.readlines()
  # to let the loop finish
  lines.append('Bin\n')
  Rows_notDetected = True
  Type = 0
  Results = {}
  for line in lines:
    if 'Bin' in line:
      if Type > 0: 
        TempCon['Bins'] = truthBins
        print(Type)
        Results[GetType(Type)] = TempCon 
      Type += 1
      TempCon = {}
      truthBins = []
      if Rows_notDetected:
        Rows = [ x.strip(' ') for x in line.split(",")[1:-1] ]
        Rows_notDetected = False
    if '_PTV_' in line: 
      Numbs = [ x.strip(' ') for x in line.split(",")[1:-1] ]
      truthBin = line.split(",")[0].split('_PTV_')[1]
      TempCon[truthBin] = dict(list(zip(Rows,Numbs)))
      truthBins.append(truthBin)
      #print Numbs
      #exit()
    #  print line
  theF.close()
  return Results,Rows

def Merge2DHist(Hist1,Hist2,xs1,xs2,histName,outputplot):
  nX1 = Hist1.GetXaxis().GetNbins()
  nY1 = Hist1.GetYaxis().GetNbins()
  nX2 = Hist2.GetXaxis().GetNbins()
  nY2 = Hist2.GetYaxis().GetNbins()
  if   histName == 'WH':   prefix = 'QQ2HLNUxPTV'
  elif histName == 'qqZH': prefix = 'QQ2HLLxPTV'
  elif histName == 'ggZH': prefix = 'GG2HLLxPTV'

  if nX1 != nX2:
    print('The numbers of x axis are not equal! Existing')
    exit()
  if nY1 != nY2:
    print('The numbers of y axis are not equal! Existing')
    exit()
  hist2D = ROOT.TH2D('QCDDiff2DMerge_'+histName, 'QCDDiff2DMerge_'+histName+';truth bin;Deltas;', nX1, 0, nX1, nY1, 0, nY1)
  for ix in range(nX1):
    xBinName = Hist1.GetXaxis().GetBinLabel(ix+1)
    hist2D.GetXaxis().SetBinLabel(ix+1,prefix+'x'+xBinName.replace('_', 'x'))
    for iy in range(nY1):
      if ix == 0:
        yBinName = Hist1.GetYaxis().GetBinLabel(iy+1)
        hist2D.GetYaxis().SetBinLabel(iy+1,yBinName)
      content1 = Hist1.GetBinContent(ix+1,iy+1)
      content2 = Hist2.GetBinContent(ix+1,iy+1)
      contentMerge = ( content1*xs1 + content2*xs2 )/( xs1 + xs2 )
      #hist2D.SetBinContent(ix+1,iy+1,contentMerge)
      #if math.fabs(contentMerge)>0.005:
      hist2D.SetBinContent( ix+1, iy+1, contentMerge )
  can_width = 1600
  can_height = 800
  RightMargin = 0.2
  BotMargin = 0.2
  c1 = ROOT.TCanvas('PDFDiff'+histName,'PDFDiff'+histName,can_width,can_height)
  hist2D.Draw('TEXT')
  c1.SetRightMargin(RightMargin)
  c1.SetBottomMargin(BotMargin)
  c1.Print(outputplot+'.%s.png'%histName)
  return hist2D

def Fixhist(Hist1,histName, outputplot= "QCDDiffqqWH"):
    nX1 = Hist1.GetXaxis().GetNbins()
    nY1 = Hist1.GetYaxis().GetNbins()
    if   histName == 'WH':   prefix = 'QQ2HLNUxPTV'
    hist = ROOT.TH2D('QCDDiff2DMerge_'+histName, 'QCDDiff2DMerge_'+histName+';truth bin;Deltas;', nX1, 0, nX1, nY1, 0, nY1)
    
    # hist = ROOT.TH2F(f'PDF_For_{histName}', "PDFDiff2DMerge", len(max_split), 0, len(max_split)+1, len(hist_list.keys()), 0, len(hist_list.keys())+1)
    hist.SetStats(0)
    for ix in range(nX1):
        xBinName = Hist1.GetXaxis().GetBinLabel(ix+1)
        hist.GetXaxis().SetBinLabel(ix+1,prefix+'x'+xBinName.replace('_', 'x')) 
        for iy in range(nY1):
            if ix == 0:
                yBinName = Hist1.GetYaxis().GetBinLabel(iy+1)
                hist.GetYaxis().SetBinLabel(iy+1,yBinName.replace('PDF', 'SysTheoryPDF_').replace('alphaS', 'SysTheoryalphaS'))
            content1 = Hist1.GetBinContent(ix+1,iy+1)
            contentMerge = content1
            hist.SetBinContent( ix+1, iy+1, contentMerge )
    can_width = 1600
    can_height = 800
    RightMargin = 0.2
    BotMargin = 0.2
    c1 = ROOT.TCanvas('PDFDiff'+histName,'PDFDiff'+histName,can_width,can_height)
    hist.Draw('TEXT')
    c1.SetRightMargin(RightMargin)
    c1.SetBottomMargin(BotMargin)
    c1.Print(outputplot+'.%s.png'%histName)
    return hist


def main():
  res,rows = getResults('QCDScaleVariationCalculation.Nicest.v16/Relative.Error.From.QCD.Variation.With.FrankWith0.5.txt')
  hist2D={}
  nBinsY = len(rows) - 10
  # plotting settings:
  can_width = 1200
  can_height = 600
  for sig in res:
    print(sig) 
    Binx_sig    = res[sig]['Bins']
    nBinsX      = len(Binx_sig)
    hist2D[sig] = ROOT.TH2D( 'QCDDiff2D_'+sig, 'QCDDiff_'+sig, nBinsX, 0, nBinsX, nBinsY, 0, nBinsY )
    # set label x
    for ibin_x in range(nBinsX):
      hist2D[sig].GetXaxis().SetBinLabel(ibin_x+1,Binx_sig[ibin_x])
    # set label y
    for ibin_y in range(nBinsY):
      hist2D[sig].GetYaxis().SetBinLabel(ibin_y+1,rows[ibin_y])
    for BinName in Binx_sig:
      for SysName in rows:
        ix = Binx_sig.index(BinName)
        iy = rows.index(SysName)
        if '_1' in SysName: iy = rows.index('0_1')
        elif '_2' in SysName: iy = rows.index('0_2')
        content = res[sig][BinName][SysName]
        if content == '-': continue
        else : number = float(content)
        hist2D[sig].SetBinContent(ix+1,iy+1,number)
        #print content
    c1=ROOT.TCanvas( 'QCDDiff'+sig, 'QCDDiff'+sig, can_width, can_height)
    hist2D[sig].Draw('TEXT')
    c1.Print('QCDDiff'+sig+'.png')
  outputplot = 'QCDDiff'
  outputfile = ROOT.TFile(outputplot+'.root','recreate')
  # Merge2DHist( hist2D['qqWlvH125M'], hist2D['qqWlvH125P'], getXS('qqWlvH125M'), getXS('qqWlvH125P'), 'WH',   outputplot ).Write()
  Fixhist(hist2D['qqWlvH125'], 'WH').Write()
  Merge2DHist( hist2D['qqZllH125'],  hist2D['qqZvvH125'],  getXS('qqZllH125' ), getXS('qqZvvH125' ), 'qqZH', outputplot ).Write()
  Merge2DHist( hist2D['ggZllH125'],  hist2D['ggZvvH125'],  getXS('ggZllH125' ), getXS('ggZvvH125' ), 'ggZH', outputplot ).Write()
  outputfile.Close()


if __name__ == '__main__':
  main()
