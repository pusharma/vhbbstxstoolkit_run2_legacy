import collections
from ROOT import *
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)
gROOT.ForceStyle()
gStyle.SetPaintTextFormat("1.2f")
gROOT.LoadMacro("macros/AtlasStyle.C")
gROOT.LoadMacro("macros/AtlasUtils.C")
gROOT.LoadMacro("macros/AtlasLabels.C")
SetAtlasStyle()
def Convet2DHistToDictionary(Hist1,flag):
  Variation = collections.OrderedDict()
  nX1 = Hist1.GetXaxis().GetNbins()
  nY1 = Hist1.GetYaxis().GetNbins()
  VarNames = []
  BinNames = []
  for ix in range(nX1):
    xBinName = Hist1.GetXaxis().GetBinLabel(ix+1)
    BinNames.append(xBinName.replace('_','x').replace('2HLNU','WH').replace('2HLL','ZH').replace('2HNUNU','ZH'))
    truthBinVar = collections.OrderedDict()
    for iy in range(nY1):
      yBinName = Hist1.GetYaxis().GetBinLabel(iy+1)
      if flag:
        yBinName = f'SysTheoryQCDScaleDelta{yBinName}'
      if yBinName in ['Nominal','alphaSdn']: continue
      if ix == 0: VarNames.append(yBinName)
      content = Hist1.GetBinContent(ix+1,iy+1)
      truthBinVar[yBinName] = content
    Variation[xBinName.replace('2HLNU','WH').replace('2HLL','ZH').replace('2HNUNU','ZH')] = truthBinVar
  return Variation,VarNames,BinNames
def GenerateQCDScalePlot(h_dict,name,signal,Plotrange):
    variations = ['Y', '75', '150', '250', '400', '600']
    mycolors = [kMagenta, kOrange+1,kGreen+2, kBlue,kRed, kTeal -1, kViolet, kAzure+10]
    old_var = {}
    for i in variations:
        old_var[i] = {}
    for i in variations:
        for keys, values in h_dict[0].items():
            old_var[i][keys] = values[i]
    
    c = TCanvas('c', 'c', 800, 600)
    c.SetBottomMargin(0.3)
    c.SetRightMargin(0.2)
    c.SetLeftMargin(0.2)

    hist_list=[]
    rangedown = Plotrange[0]
    rangeup = Plotrange[1]


    legend=TLegend(0.805,0.3,0.97,0.95)
    legend.SetBorderSize(1)
    for k,i in enumerate(old_var):
        h = TH1F(f'{i}', f'{i}', len(old_var[i].keys()), 0,len(old_var[i].keys()))
        for keys, values in old_var[i].items():
            h.Fill(keys, values)    
        h.SetMaximum(rangeup)
        h.SetMinimum(rangedown)
        h.GetXaxis().LabelsOption("v")
        h.GetYaxis().SetLabelSize(0.03)
        h.GetXaxis().SetLabelSize(0.03)
        h.GetYaxis().SetTitle(r"Relative QCDScale uncertainty")
        h.SetStats(0)        
        h.SetLineColor(mycolors[k])
        h.SetMarkerColor(mycolors[k])
        h.SetMarkerStyle(20)
        h.SetMarkerSize(0.5)
        h.SetLineWidth(3)
        legend.AddEntry(h, f'QCDScaleDelta_{i}', "l")
        h.SetLineColor(mycolors[k])
        hist_list.append(h)
    for i in hist_list:
        i.Draw("hist same")
    ATLASLabel( 0.35, 0.85,"Simulation Internal", 1 )
    if signal == "WH":
        myText( 0.35, 0.80, 1, r"qq \rightarrow WHbb" )
    elif signal == "qqZH":
        myText( 0.35, 0.80, 1, r"qq \rightarrow ZHbb" )
    elif signal == "ggZH":
        myText( 0.35, 0.80, 1, r"gg \rightarrow ZHbb" )
    legend.Draw('same')
    c.SaveAs(f'{name}.pdf')

def GenerateNJetPlot(h_dict,name,signal,Plotrange):
    variations = ['0_1','0_2']
    mycolors = [kViolet, kAzure+10]
    old_var = {}
    for i in variations:
        old_var[i] = {}
    for i in variations:
        for keys, values in h_dict[0].items():
            old_var[i][keys] = values[i]
    
    c = TCanvas('c', 'c', 800, 600)
    c.SetBottomMargin(0.3)
    c.SetRightMargin(0.2)
    c.SetLeftMargin(0.2)

    hist_list=[]
    rangedown = Plotrange[0]
    rangeup = Plotrange[1]


    legend=TLegend(0.805,0.3,0.97,0.95)
    legend.SetBorderSize(1)
    for k,i in enumerate(old_var):
        h = TH1F(f'{i}', f'{i}', len(old_var[i].keys()), 0,len(old_var[i].keys()))
        for keys, values in old_var[i].items():
            h.Fill(keys, values)    
        h.SetMaximum(rangeup)
        h.SetMinimum(rangedown)
        h.GetXaxis().LabelsOption("v")
        h.GetYaxis().SetLabelSize(0.03)
        h.GetXaxis().SetLabelSize(0.03)
        h.GetYaxis().SetTitle(r"Relative QCD Njet uncertainty")
        h.SetStats(0)        
        h.SetLineColor(mycolors[k])
        h.SetMarkerColor(mycolors[k])
        h.SetMarkerStyle(20)
        h.SetMarkerSize(0.5)
        h.SetLineWidth(3)
        legend.AddEntry(h, f'QCDScaleDelta_{i.split("_")[1]}', "l")
        h.SetLineColor(mycolors[k])
        hist_list.append(h)
    for i in hist_list:
        i.Draw("hist same")
    ATLASLabel( 0.35, 0.85,"Simulation Internal", 1 )
    if signal == "WH":
        myText( 0.35, 0.80, 1, r"qq \rightarrow WHbb" )
    elif signal == "qqZH":
        myText( 0.35, 0.80, 1, r"qq \rightarrow ZHbb" )
    elif signal == "ggZH":
        myText( 0.35, 0.80, 1, r"gg \rightarrow ZHbb" )
    legend.Draw('same')
    c.SaveAs(f'{name}.pdf')
infile=TFile.Open('/usatlas/u/pusharma/usatlasdata/vhbb/stxs_uncertainity/vhbbstxstoolkit_run2_legacy/new_output/QCDDiff.root')
h1= infile.Get('QCDDiff2DMerge_qqZH')
h1_dict = Convet2DHistToDictionary(h1,0)
GenerateQCDScalePlot(h1_dict,"QCDDiff2DMerge_qqZH",'qqZH',[-0.05,0.1])
GenerateNJetPlot(h1_dict,"QCDScale_NJet_qqZH",'qqZH',[-0.2,0.2])

h1= infile.Get('QCDDiff2DMerge_ggZH')
h1_dict = Convet2DHistToDictionary(h1,0)
GenerateQCDScalePlot(h1_dict,"QCDDiff2DMerge_ggZH",'ggZH',[-1.5,1.0])
GenerateNJetPlot(h1_dict,"QCDScale_NJet_ggZH",'ggZH',[-1.0,0.7])

h1= infile.Get('QCDDiff2DMerge_WH')
h1_dict = Convet2DHistToDictionary(h1,0)
GenerateQCDScalePlot(h1_dict,"QCDDiff2DMerge_WH",'WH',[-0.05,0.07])
GenerateNJetPlot(h1_dict,"QCDScale_NJet_WH",'WH',[-0.2,0.2])