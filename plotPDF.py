from ROOT import *
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)
gROOT.ForceStyle()
gStyle.SetPaintTextFormat("1.2f")
gROOT.LoadMacro("macros/AtlasStyle.C")
gROOT.LoadMacro("macros/AtlasUtils.C")
gROOT.LoadMacro("macros/AtlasLabels.C")
SetAtlasStyle()
infile=TFile.Open("/usatlas/u/pusharma/usatlasdata/vhbb/stxs_uncertainity/vhbbstxstoolkit_run2_legacy/out_PDF_raw.root")
for keys in infile.GetListOfKeys():
    print(keys.GetName())
variation = ["PDF00","PDF01", "PDF02", "PDF03", "PDF04", "PDF05", "PDF06", "PDF07", "PDF08", "PDF09", "PDF10", "PDF11", "PDF12", "PDF13", "PDF14", "PDF15", "PDF16", "PDF17", "PDF18", "PDF19", "PDF20", "PDF21", "PDF22", "PDF23", "PDF24", "PDF25", "PDF26", "PDF27", "PDF28", "PDF29", "PDF30", "alphaSup","alphaSdn"]
mycolors = [kRed, kRed+1, kRed+2, kRed+3, kBlue, kBlue-1, kBlue-2, kBlue+1, kBlue+2, kBlue+3, kBlue+3, kViolet, kViolet+1, kMagenta, kPink, kOrange, kYellow,  kYellow+1, kSpring, kGreen, kGreen-1, kGreen-2, kGreen+1, kGreen+2, kGreen+3, kTeal, kCyan, kCyan+1, kAzure, kGray, kGray+1, kGray+3, kGray+4]
import collections
def Convet2DHistToDictionary(Hist1,flag):
  Variation = collections.OrderedDict()
  nX1 = Hist1.GetXaxis().GetNbins()
  nY1 = Hist1.GetYaxis().GetNbins()
  VarNames = []
  BinNames = []
  for ix in range(nX1):
    xBinName = Hist1.GetXaxis().GetBinLabel(ix+1)
    BinNames.append(xBinName.replace('_','x'))
    truthBinVar = collections.OrderedDict()
    for iy in range(nY1):
      yBinName = Hist1.GetYaxis().GetBinLabel(iy+1)
      if flag:
        yBinName = f'SysTheoryQCDScaleDelta{yBinName}'
      if yBinName in ['Nominal','alphaSdn']: continue
      if ix == 0: VarNames.append(yBinName)
      content = Hist1.GetBinContent(ix+1,iy+1)
      truthBinVar[yBinName] = content
    Variation[xBinName] = truthBinVar
  return Variation,VarNames,BinNames
def GeneratePDFPlot_NEW(h_dict,name,signal):
    variations = ['SysTheoryPDF_01', 'SysTheoryPDF_02', 'SysTheoryPDF_03', 'SysTheoryPDF_04', 'SysTheoryPDF_05', 'SysTheoryPDF_06', 'SysTheoryPDF_07', 'SysTheoryPDF_08', 'SysTheoryPDF_09', 'SysTheoryPDF_10', 'SysTheoryPDF_11', 'SysTheoryPDF_12', 'SysTheoryPDF_13', 'SysTheoryPDF_14', 'SysTheoryPDF_15', 'SysTheoryPDF_16', 'SysTheoryPDF_17', 'SysTheoryPDF_18', 'SysTheoryPDF_19', 'SysTheoryPDF_20', 'SysTheoryPDF_21', 'SysTheoryPDF_22', 'SysTheoryPDF_23', 'SysTheoryPDF_24', 'SysTheoryPDF_25', 'SysTheoryPDF_26', 'SysTheoryPDF_27', 'SysTheoryPDF_28', 'SysTheoryPDF_29', 'SysTheoryPDF_30', 'SysTheoryalphas']
    # variations = ['SysTheoryPDF_00', 'SysTheoryPDF_02', 'SysTheoryPDF_03', 'SysTheoryPDF_04', 'SysTheoryPDF_05', 'SysTheoryPDF_06', 'SysTheoryPDF_07', 'SysTheoryPDF_08', 'SysTheoryPDF_09', 'SysTheoryPDF_10', 'SysTheoryPDF_11', 'SysTheoryPDF_12', 'SysTheoryPDF_13', 'SysTheoryPDF_14', 'SysTheoryPDF_15', 'SysTheoryPDF_16', 'SysTheoryPDF_17', 'SysTheoryPDF_18', 'SysTheoryPDF_19', 'SysTheoryPDF_20', 'SysTheoryPDF_21', 'SysTheoryPDF_22', 'SysTheoryPDF_23', 'SysTheoryPDF_24', 'SysTheoryPDF_25', 'SysTheoryPDF_26', 'SysTheoryPDF_27', 'SysTheoryPDF_28', 'SysTheoryPDF_29', 'SysTheoryPDF_30', 'SysTheoryalphas']
    mycolors = [kRed, kRed+1, kRed+2, kRed+3, kBlue, kBlue-1, kBlue-2, kBlue+1, kBlue+2, kBlue+3, kBlue+3, kViolet, kViolet+1, kMagenta, kPink, kOrange, kYellow,  kYellow+1, kSpring, kGreen, kGreen-1, kGreen-2, kGreen+1, kGreen+2, kGreen+3, kTeal, kCyan, kAzure, kGray, kGray+1, kGray+3]
    old_var = {}
    for i in variations:
        old_var[i] = {}
    for i in variations:
        for keys, values in h_dict[0].items():
            old_var[i][keys] = values[i]
    
    c = TCanvas('c', 'c', 800, 600)
    c.SetBottomMargin(0.3)
    c.SetRightMargin(0.2)
    c.SetLeftMargin(0.2)

    hist_list=[]


    legend=TLegend(0.805,0.3,0.97,0.95)
    legend.SetBorderSize(1)
    for k,i in enumerate(old_var):
        h = TH1F(f'{i}', f'{i}', len(old_var[i].keys()), 0,len(old_var[i].keys()))
        for keys, values in old_var[i].items():
            h.Fill(keys, values)    
        h.SetMaximum(0.1)
        h.SetMinimum(-0.1)
        h.GetXaxis().LabelsOption("v")
        h.GetYaxis().SetLabelSize(0.03)
        h.GetXaxis().SetLabelSize(0.03)
        h.GetYaxis().SetTitle(r"Relative PDF, \alpha_{S} uncertainty")
        h.SetStats(0)        
        h.SetLineColor(mycolors[k])
        h.SetMarkerColor(mycolors[k])
        h.SetMarkerStyle(20)
        h.SetMarkerSize(0.5)
        h.SetLineWidth(2)
        legend.AddEntry(h, i, "l")
        h.SetLineColor(mycolors[k])
        hist_list.append(h)
    for i in hist_list:
        i.Draw("hist same")
    ATLASLabel( 0.35, 0.85,"Simulation Internal", 1 )
    if signal == "WH":
        myText( 0.35, 0.80, 1, r"qq \rightarrow WHbb" )
    elif signal == "qqZH":
        myText( 0.35, 0.80, 1, r"qq \rightarrow ZHbb" )
    elif signal == "ggZH":
        myText( 0.35, 0.80, 1, r"gg \rightarrow ZHbb" )
    legend.Draw('same')
    c.SaveAs(f'{name}.pdf')  
h1= infile.Get('PDF_for_qqZH')
h2= infile.Get('PDF_for_ggZH')
h3= infile.Get('PDF_for_WH')

GeneratePDFPlot_NEW(Convet2DHistToDictionary(h1,0),'PDF_qqZH','qqZH')
GeneratePDFPlot_NEW(Convet2DHistToDictionary(h2,0),'PDF_ggZH','ggZH')
GeneratePDFPlot_NEW(Convet2DHistToDictionary(h3,0),'PDF_qqWH','WH')