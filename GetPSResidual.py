import ROOT
# infile=ROOT.TFile.Open("/gpfs/mnt/atlasgpfs01/usatlas/data/pusharma/vhbb/stxs_uncertainity/inputs/hadded_noReco_25032024.root")
infile=ROOT.TFile.Open("/gpfs/mnt/atlasgpfs01/usatlas/data/pusharma/vhbb/stxs_uncertainity/inputs/hadded_noreco_nominal_noPDFRW_22072024.root")
infile_alt=ROOT.TFile.Open("/gpfs/mnt/atlasgpfs01/usatlas/data/pusharma/vhbb/stxs_uncertainity/inputs/hadded_noreco_alt_noPDFRW_22072024.root")


h0_name='ggZllH125_Stage1Bin_RecoMapEPSNicest'
hists=['ggZvvH125_Stage1Bin_RecoMapEPSNicest','qqWlvH125_Stage1Bin_RecoMapEPSNicest','qqZllH125_Stage1Bin_RecoMapEPSNicest','qqZvvH125_Stage1Bin_RecoMapEPSNicest']

fwdh_dict={
    'ggZllH125_Stage1Bin_RecoMapEPSNicest':'GG2HLL_FWDH',
    'ggZvvH125_Stage1Bin_RecoMapEPSNicest':'GG2HNUNU_FWDH',
    'qqWlvH125_Stage1Bin_RecoMapEPSNicest':'QQ2HLNU_FWDH',
    'qqZllH125_Stage1Bin_RecoMapEPSNicest':'QQ2HLL_FWDH',
    'qqZvvH125_Stage1Bin_RecoMapEPSNicest':'QQ2HNUNU_FWDH'
}


temp=infile_alt.Get(f'{h0_name}')
bin = temp.GetXaxis().FindBin("TOTAL")  # Find the bin with label "Total"
content = temp.GetBinContent(bin)  # Get the content of the bin
temp.SetBinContent(bin,0)
bin_FWDH = temp.GetXaxis().FindBin(fwdh_dict[f'{h0_name}'])  # Find the bin with label "Total"
content_FWDH=temp.GetBinContent(bin_FWDH)
# temp.SetBinContent(bin_FWDH,0)




nom_hist_from_file= infile.Get(f'{h0_name}')
bin = nom_hist_from_file.GetXaxis().FindBin("TOTAL")  # Find the bin with label "Total"
bin_FWDH = nom_hist_from_file.GetXaxis().FindBin(fwdh_dict[f'{h0_name}'])  # Find the bin with label "Total"
nom_hist_from_file.SetBinContent(bin,0)
# nom_hist_from_file.SetBinContent(bin_FWDH,0)
temp.Scale(nom_hist_from_file.Integral()/temp.Integral())
hist_alt=temp.Clone()
del temp




for i in hists:
    temp=infile_alt.Get(f'{i}')
    bin = temp.GetXaxis().FindBin("TOTAL")  # Find the bin with label "Total"
    content = temp.GetBinContent(bin)  # Get the content of the bin
    temp.SetBinContent(bin,0)
    bin_FWDH = temp.GetXaxis().FindBin(fwdh_dict[f'{i}'])  # Find the bin with label "Total"
    # temp.SetBinContent(bin_FWDH,0)
    nom_hist_from_file= infile.Get(f'{i}')
    bin_FWDH = nom_hist_from_file.GetXaxis().FindBin(fwdh_dict[f'{i}'])  # Find the bin with label "Total"
    bin = nom_hist_from_file.GetXaxis().FindBin("TOTAL")  # Find the bin with label "Total"
    nom_hist_from_file.SetBinContent(bin,0)
    # nom_hist_from_file.SetBinContent(bin_FWDH,0)
    temp.Scale(nom_hist_from_file.Integral()/temp.Integral())
    hist_alt.Add(temp)
    del temp


hist_nom=infile.Get(f'{h0_name}').Clone()
for i in hists:
    hist_nom.Add(infile.Get(f'{i}'))


def getXS(sigName):
  if sigName == 'QQ2HLNU': return 0.46227
  elif sigName == 'QQ2HLL' : return 0.07704
  elif sigName == 'GG2HLL' : return 0.01242
  elif sigName == 'QQ2HNUNU' : return 0.15305
  elif sigName == 'GG2HNUNU' : return 0.02457
  
signals = ['QQ2HLNU','QQ2HLL','QQ2HNUNU','GG2HLL','GG2HNUNU']

groupings={'QQZH':['QQ2HLL','QQ2HNUNU'],
           'GGZH':['GG2HLL','GG2HNUNU'],
           'WH':['QQ2HLNU']}


max_split=[ '_PTV_0_75_0J', '_PTV_0_75_1J', '_PTV_0_75_GE2J', '_PTV_75_150_0J', '_PTV_75_150_1J', '_PTV_75_150_GE2J', '_PTV_150_250_0J', '_PTV_150_250_1J', '_PTV_150_250_GE2J', '_PTV_250_400_0J', '_PTV_250_400_1J', '_PTV_250_400_GE2J', '_PTV_400_600_0J', '_PTV_400_600_1J', '_PTV_400_600_GE2J', '_PTV_GT600_0J', '_PTV_GT600_1J', '_PTV_GT600_GE2J','_FWDH']
max_split_old=[ '_PTV_0_75_0J', '_PTV_0_75_1J', '_PTV_0_75_GE2J', '_PTV_75_150_0J', '_PTV_75_150_1J', '_PTV_75_150_GE2J', '_PTV_150_250_0J', '_PTV_150_250_1J', '_PTV_150_250_GE2J', '_PTV_250_400_0J', '_PTV_250_400_1J', '_PTV_250_400_GE2J', '_PTV_400_600_0J', '_PTV_400_600_1J', '_PTV_400_600_GE2J', '_PTV_GT600_0J', '_PTV_GT600_1J', '_PTV_GT600_GE2J','_FWDH']
max_split_new=[ '_0_75PTV_0J', '_0_75PTV_1J', '_0_75PTV_GE2J', '_75_150PTV_0J', '_75_150PTV_1J', '_75_150PTV_GE2J', '_150_250PTV_0J', '_150_250PTV_1J', '_150_250PTV_GE2J', '_250_400PTV_0J', '_250_400PTV_1J', '_250_400PTV_GE2J', '_400_600PTV_0J', '_400_600PTV_1J', '_400_600PTV_GE2J', '_GT600PTV_0J', '_GT600PTV_1J', '_GT600PTV_GE2J','_FWDH']
max_split=max_split_new
mapping_dict = {old: new for old, new in zip(max_split_old, max_split_new)}

hist_unc=hist_nom.Clone()

for i in range(1, hist_nom.GetNbinsX()+1):
    if hist_nom.GetBinContent(i) == 0:
        hist_unc.SetBinContent(i,0.0)
        hist_unc.SetBinError(i,0.0)
    else:
        hist_unc.SetBinContent(i,-1 + hist_alt.GetBinContent(i)/hist_nom.GetBinContent(i))
        hist_unc.SetBinError(i,0.0)
        
f = ROOT.TFile("CheckPS.root", "RECREATE")
hist_unc.SetName("PSUncertainty")
hist_unc.Write()
hist_nom.SetName("nomYields")
hist_nom.Write()
hist_alt.SetName("altYields")
hist_alt.Write()
f.Close()

grouping ={}
Scheme=13

if Scheme == 14:
    grouping = {
    'ZHx75x150PTVx0J':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", },
    'ZHx75x150PTVxGE1J':{ "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J", },
    'ZHx150x250PTVx0J':{ "QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", },
    'ZHx150x250PTVxGE1J':{"QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTVx0J':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J", },
    'ZHx250x400PTVxGE1J':{ "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", },
    'ZHx400x600PTV':{ "QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", },
    'ZHxGT600PTV':{ "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTV':{ "QQWHx150x250PTVx0J", "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J",},
    'WHx250x400PTV':{ "QQWHx250x400PTVx0J", "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J",  },
    'WHx400x600PTV':{ "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", },
    'WHxGT600PTV':{ "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } }
elif Scheme == 13: 
    grouping = {
    'ZHx75x150PTVx0J':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", },
    'ZHx75x150PTVxGE1J':{ "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J", },
    'ZHx150x250PTVx0J':{ "QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", },
    'ZHx150x250PTVxGE1J':{"QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTVx0J':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J", },
    'ZHx250x400PTVxGE1J':{ "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", },
    'ZHx400x600PTV':{ "QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", },
    'ZHxGT600PTV':{ "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTVx0J':{ "QQWHx150x250PTVx0J", },
    'WHx150x250PTVxGE1J':{ "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J", },
    'WHx250x400PTVx0J':{ "QQWHx250x400PTVx0J", },
    'WHx250x400PTVxGE1J':{ "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J", },
    'WHx400x600PTV':{ "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", },
    'WHxGT600PTV':{ "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } }
elif Scheme == 10:
    grouping = {
    'ZHx75x150PTV':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J",},
    'ZHx150x250PTV':{"QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", "QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTV':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J","QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J",},
    'ZHx400x600PTV':{ "QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", },
    'ZHxGT600PTV':{ "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTV':{ "QQWHx150x250PTVx0J", "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J",},
    'WHx250x400PTV':{ "QQWHx250x400PTVx0J", "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J",  },
    'WHx400x600PTV':{ "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", },
    'WHxGT600PTV':{ "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } }
elif Scheme == 133:
    grouping = {
    'ZHx75x150PTVx0J':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", },
    'ZHx75x150PTVxGE1J':{ "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J", },
    'ZHx150x250PTVx0J':{ "QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", },
    'ZHx150x250PTVxGE1J':{"QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTVx0J':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J", },
    'ZHx250x400PTVxGE1J':{ "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J", },
    'ZHxGT400PTV':{"QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTVx0J':{ "QQWHx150x250PTVx0J", },
    'WHx150x250PTVxGE1J':{ "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J", },
    'WHx250x400PTVx0J':{ "QQWHx250x400PTVx0J", },
    'WHx250x400PTVxGE1J':{ "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J", },
    'WHxGT400PTV':{ "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J","QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } }
elif Scheme == 8:
    grouping = {
    'ZHx75x150PTV':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J",},
    'ZHx150x250PTV':{"QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", "QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J", },
    'ZHx250x400PTV':{ "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J","QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J",},
    'ZHxGT400PTV':{"QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHx75x150PTV':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J", },
    'WHx150x250PTV':{ "QQWHx150x250PTVx0J", "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J",},
    'WHx250x400PTV':{ "QQWHx250x400PTVx0J", "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J",  },
    'WHxGT400PTV':{"QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } 
    }
elif Scheme == 2:
    grouping = {
    'ZHbb':{ "QQZHx75x150PTVx0J", "GGZHx75x150PTVx0J", "QQZHx75x150PTVx1J", "QQZHx75x150PTVxGE2J", "GGZHx75x150PTVx1J", "GGZHx75x150PTVxGE2J",
    "QQZHx150x250PTVx0J", "GGZHx150x250PTVx0J", "QQZHx150x250PTVx1J", "QQZHx150x250PTVxGE2J", "GGZHx150x250PTVx1J", "GGZHx150x250PTVxGE2J",
    "QQZHx250x400PTVx0J", "GGZHx250x400PTVx0J","QQZHx250x400PTVx1J", "QQZHx250x400PTVxGE2J", "GGZHx250x400PTVx1J", "GGZHx250x400PTVxGE2J",
    "QQZHx400x600PTVx0J", "QQZHx400x600PTVx1J", "QQZHx400x600PTVxGE2J", "GGZHx400x600PTVx0J", "GGZHx400x600PTVx1J", "GGZHx400x600PTVxGE2J", "QQZHxGT600PTVx0J", "QQZHxGT600PTVx1J", "QQZHxGT600PTVxGE2J", "GGZHxGT600PTVx0J", "GGZHxGT600PTVx1J", "GGZHxGT600PTVxGE2J", },
    'WHbb':{ "QQWHx75x150PTVx0J", "QQWHx75x150PTVx1J", "QQWHx75x150PTVxGE2J",
    "QQWHx150x250PTVx0J", "QQWHx150x250PTVx1J", "QQWHx150x250PTVxGE2J",
    "QQWHx250x400PTVx0J", "QQWHx250x400PTVx1J", "QQWHx250x400PTVxGE2J",
    "QQWHx400x600PTVx0J", "QQWHx400x600PTVx1J", "QQWHx400x600PTVxGE2J", "QQWHxGT600PTVx0J", "QQWHxGT600PTVx1J", "QQWHxGT600PTVxGE2J", } 
    }
   
    
Scheme_poi={13:'15',14:'13',10:'10',133:'11p2',8:'8',2:'2'}



unc_hist_dict={}
for i in range(1,hist_unc.GetNbinsX()):
    unc_hist_dict[hist_unc.GetXaxis().GetBinLabel(i).replace('_','x')]=hist_unc.GetBinContent(i)




fxs = ROOT.TFile("/gpfs/mnt/atlasgpfs01/usatlas/data/pusharma/vhbb/stxs_uncertainity/vhbbstxstoolkit_run2_legacy/Initial_XS_ade.root", "READ")
debug=False
def createXSLookupTable(h, groupDic):
    xsDic = {}
    for key, value in groupDic.items():
        tmpXS = 0.0
        for val in value: 
            xsDic[val] = getCrossSection(h, val) #fill individual
            tmpXS+= getCrossSection(h, val)
        xsDic[key] = tmpXS #fill the group
    return xsDic
def getCrossSection(h, name):
    name = name.replace('x','_')
    tmpXS = 0.0
    if "QQZH" in name:
        name = name.replace('QQZH','QQ2HLL')
        tmpXS = getContentByLabel(h, name)
        name = name.replace('QQ2HLL','QQ2HNUNU')
        tmpXS += getContentByLabel(h, name)
    elif "GGZH" in name:
        name = name.replace('GGZH','GG2HLL')
        tmpXS = getContentByLabel(h, name)
        name = name.replace('GG2HLL','GG2HNUNU')
        tmpXS += getContentByLabel(h, name)
    else:
        tmpXS = getContentByLabel(h, name)
    return tmpXS
def getContentByLabel(h, name):
    if debug:
        print("Getting histogram: %s"%name)
    for iBin in range(1, h.GetNbinsX()+1):
        if name == h.GetXaxis().GetBinLabel(iBin):
            return h.GetBinContent(iBin)
    return -1


hXS = fxs.Get("Initial")
xsLookup = createXSLookupTable(hXS, grouping)

def getBinNrByLabel(h, name):
    for iBin in range(1, h.GetNbinsX()+1):
        if name == h.GetXaxis().GetBinLabel(iBin):
            return iBin
    return -1

def fillDic(h, name, dic, xsDic):
    uncName = "SysTheoryPS"
    if uncName in list(dic.keys()):
        dic[uncName] += h[name]*xsDic[name]
    else:
        dic[uncName] = h[name]*xsDic[name]

def scaleDic(uncDic, xsDic, groupName):
    for key, value in uncDic.items():
        uncDic[key] = value/xsDic[groupName]

def getGroupUncertainties(uncDic, groupDic, xsDic):
    bigDic = {}
    #which groups do we need?
    for key, value in groupDic.items():
        if debug:
            print("Group: %s"%key)
        tmpDic = {}
        for val in value:
            # print(val)
            if debug:
                print("-->getting unc. for %s"%val)
            if "QQWH" in val:
                htmp = uncDic["WH"]
            elif "QQZH" in val or "QQ2HLL" in val or "QQ2HNUNU" in val:
                htmp = uncDic["QQZH"]
            elif "GGZH" in val or "GG2HLL" in val or "GG2HNUNU" in val:
                htmp = uncDic["GGZH"]
            fillDic(htmp, val, tmpDic, xsDic)

        scaleDic(tmpDic, xsDic, key)
        bigDic[key] = tmpDic

    return bigDic




#2.2) loop over XS histograms
#----helper fkts
def calculateAndSaveResidual(h, groupDic, grUncDic):
    hnew = h.Clone()
    nBx = hnew.GetNbinsX()
    print(nBx)
    nBy = hnew.GetNbinsY()
    for iBy in range(1, nBy+1): 
        for iBx in range(1, nBx+1):
            uncName = 'SysTheoryPS'
            binName = hnew.GetXaxis().GetBinLabel(iBx)
            print(binName)
            delta = hnew.GetBinContent(iBx, iBy)
            groupDelta = 0.0
            residual = 0.0
            groupName = ""
            
            for key, value in groupDic.items():
                for val in value:
                    if val == binName:
                        groupName = key
            if groupName != "":
                groupDelta = grUncDic[groupName][uncName]
                binNr = hnew.GetBin(iBx, iBy)
                residual = (delta - groupDelta)
                hnew.SetBinContent(binNr, residual)
            else:
                binNr = hnew.GetBin(iBx, iBy)
                hnew.SetBinContent(binNr, 0.0)
                
            
            if groupName != "": 
                print("Getting residual uncertainty")
                print("Uncertainty name: %s"%uncName)
                print("Bin Name: %s"%binName)
                print("Group Name: %s"%groupName)
                print("Delta: %f"%delta)
                print("Delta Group: %f"%groupDelta)
                print("Residual: %f"%residual)

             
    hnew.Write()
    
def calculateAndSaveWithoutResidual(h):
    hnew = h.Clone()
    nBx = hnew.GetNbinsX()
    nBy = hnew.GetNbinsY()
    for iBy in range(1, nBy+1): 
        for iBx in range(1, nBx+1):
            uncName = 'SysTheoryPS'
            binName = hnew.GetXaxis().GetBinLabel(iBx)
            binNr = hnew.GetBin(iBx, iBy)            
            delta = hnew.GetBinContent(iBx, iBy)
            hnew.SetBinContent(binNr, delta)
             
    hnew.Write()




unc_dict={}
for i in hist_unc.GetXaxis().GetLabels():
    label=i.GetName()
    if i == 'TOTAL': continue
    else:
        unc_dict[label.replace('_','x')]=hist_unc.GetBinContent(getBinNrByLabel(hist_unc,i))
grouped_unc_dict={}
xsLookup_local={}
for i in hXS.GetXaxis().GetLabels():
    label=i.GetName()
    xsLookup_local[label.replace('_','x')]=hXS.GetBinContent(getBinNrByLabel(hXS,i))
for i in groupings:
    grouped_unc_dict[i]={}
    for k in groupings[i]:
        for l in unc_dict:
            if k in l:
                grouped_unc_dict[i][l]=unc_dict[l]
unc = grouped_unc_dict
groupings={'QQZH':['QQ2HLL','QQ2HNUNU'],
           'GGZH':['GG2HLL','GG2HNUNU'],
           'WH':['QQ2HLNU']}

max_split_old=[ '_PTV_0_75_0J', '_PTV_0_75_1J', '_PTV_0_75_GE2J', '_PTV_75_150_0J', '_PTV_75_150_1J', '_PTV_75_150_GE2J', '_PTV_150_250_0J', '_PTV_150_250_1J', '_PTV_150_250_GE2J', '_PTV_250_400_0J', '_PTV_250_400_1J', '_PTV_250_400_GE2J', '_PTV_400_600_0J', '_PTV_400_600_1J', '_PTV_400_600_GE2J', '_PTV_GT600_0J', '_PTV_GT600_1J', '_PTV_GT600_GE2J','_FWDH']
max_split_new=[ '_0_75PTV_0J', '_0_75PTV_1J', '_0_75PTV_GE2J', '_75_150PTV_0J', '_75_150PTV_1J', '_75_150PTV_GE2J', '_150_250PTV_0J', '_150_250PTV_1J', '_150_250PTV_GE2J', '_250_400PTV_0J', '_250_400PTV_1J', '_250_400PTV_GE2J', '_400_600PTV_0J', '_400_600PTV_1J', '_400_600PTV_GE2J', '_GT600PTV_0J', '_GT600PTV_1J', '_GT600PTV_GE2J','_FWDH']


temp_list=[]
for i in max_split_old:
    temp_list.append(i.replace('_','x'))


temp_list_new=[]
for i in max_split_new:
    temp_list_new.append(i.replace('_','x'))

mapping_dict = {old: new for old, new in zip(temp_list, temp_list_new)}


# for i in unc:
#     check_unc[i]={}
#     for j in unc[i]:
#         print(i,j)
check_unc={}
for i in ['QQZH','GGZH']:
    check_unc[i]={}
    for j in temp_list:
        # print(f'{groupings[i][0]}{j}', f'{groupings[i][1]}{j}')
        # print(xsLookup_local[f'{groupings[i][0]}{j}'],xsLookup_local[f'{groupings[i][1]}{j}'])
        check_unc[i][f'{i}{mapping_dict[j]}']= (xsLookup_local[f'{groupings[i][0]}{j}']*unc[i][f'{groupings[i][0]}{j}'] + xsLookup_local[f'{groupings[i][1]}{j}']*unc[i][f'{groupings[i][1]}{j}'] ) / (xsLookup_local[f'{groupings[i][0]}{j}'] + xsLookup_local[f'{groupings[i][1]}{j}'])
        
check_unc['WH']={}
for i in unc['WH']:
    temp_string=i
    check_unc['WH']['QQWH'+mapping_dict[i.replace('QQ2HLNU','')]]=unc['WH'][i]


groupUncDic = getGroupUncertainties(check_unc, grouping, xsLookup)
DoResiduals=False
if DoResiduals==True:
    outName=f"out_PS_{Scheme_poi[Scheme]}POI_residuals.root"
else:
    outName="out_PS_raw.root"

fout = ROOT.TFile(outName,"recreate")
for key, value in check_unc.items():
    print("Calculating and saving residuals for %s"%key)
    h = ROOT.TH2F(f"PS_for_{key.replace('GG','gg').replace('QQ','qq')}", f"PS_for_{key.replace('GG','gg').replace('QQ','qq')}", len(value), 0, len(value), 1, 0, 1)
    h.GetYaxis().SetBinLabel(1, "SysTheoryPS")
    for i, (key, val) in enumerate(value.items()):
        h.GetXaxis().SetBinLabel(i+1, key)
        h.SetBinContent(i+1,1, val)
    if DoResiduals==True:
        calculateAndSaveResidual(h, grouping, groupUncDic)
    else:
        calculateAndSaveWithoutResidual(h)
fout.Close()